Paymeng gateway module for Ubercart and Intellectmoney.ru

This module is use some API code fragments from original Intellectmoney module. 
See http://wiki.intellectmoney.ru/display/eshops/Drupal.

There is some enhancements and errors fixed compared to original module:
	- payments logging to database including raw requests
	- fixed error with final order payment and cart clearance
	- payment is made not for a order_total but for the current order balance
	- added checks for exluding repeating payments (fraud)
	- closing order only when order balance is 0

Installation
1. download and install module
2. enable "IntellectMoney Gateway" module in /admin/modules
3. Go to Store -> Configureation -> Payment methods and press settings or 
go to admin/store/settings/payment/method/detovik_im directly
4. Login to you profile on intellectmoney.ru
5. Copy Result URL from your site to intellectmoney.ru profile's Result URL
6. Fill and copy other settings from intellectmoney.ru profile to your site
7. Enable testing mode and test payments (HIGHLY RECOMMENDED)
8. Switch to production mode
